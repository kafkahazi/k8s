# K8s

##0. Add Bitnami repo

```
helm repo add bitnami https://charts.bitnami.com/bitnami
```

##1. Install cassandra

```
helm install cassandra --namespace kafka-hazi --set dbUser.password=cassandra,cluster.datacenter=datacenter1 bitnami/cassandra
```

open pod cli, then create keyspace & advertisement table

```
# cqlsh -u cassandra -p cassandra

# CREATE KEYSPACE hazi WITH REPLICATION = {'class': 'SimpleStrategy', 'replication_factor': 1};

# CREATE TABLE hazi.advertisement (id uuid PRIMARY KEY,name text,message text);
```

##2. Install Kafka

```
helm install kafka --namespace kafka-hazi --set autoCreateTopicsEnable=true bitnami/kafka
```

## -- HELM --

if you use helm, skip the 3,4,5 chapters.

```
# cd helm
# helm install <release-name> ./hazi-app

```

##3. Install Backend

```
kubectl apply -f backend.yml
```

##4. Install Middleware

```
kubectl apply -f middleware.yml
```

##5. Install Frontend

```
kubectl apply -f frontend.yml
```

Frontend application available on `localhost:31000`